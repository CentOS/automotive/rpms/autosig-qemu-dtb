Name:           autosig-qemu-dtb
Version:        0.2
Release:        1%{?dist}
Summary:        Dtb for qemu

# This is the qemu licences. Don't know what the actual output file license really is
License:        GPLv2 and BSD and MIT and CC-BY
Source0:        qemu-kvm.dts
Source1:        qemu-tcg.dts

BuildRequires:  dtc

# No dtbs in x86-land so it makes sense
ExclusiveArch:  aarch64

%description
Create dtb for use with aarch64 qemu, in combination with autosig-u-boot firmware.

%build

%install
# SOURCE0 and SOURCE1 generated via (with 4 cores as a reasonable minimum for portability):
#   qemu-system-aarch64 -smp 4 -machine virt,accel=kvm -m 4G -cpu max -bios /boot/u-boot.bin -machine dumpdtb=qemu-kvm.dtb
#   qemu-system-aarch64 -smp 4 -machine virt,accel=tcg -m 4G -cpu max -bios /boot/u-boot.bin -machine dumpdtb=qemu-tcg.dtb
# on Mac Mini M1
#
# and decompiled using:
#   dtc -I dtb qemu-kvm.dtb > qemu-kvm.dts
#   dtc -I dtb qemu-tcg.dtb > qemu-tcg.dts

mkdir -p %{buildroot}%{_datadir}/qemu/
dtc %{SOURCE0} -o %{buildroot}%{_datadir}/qemu/qemu-kvm.dtb
dtc %{SOURCE1} -o %{buildroot}%{_datadir}/qemu/qemu-tcg.dtb

%files
%{_datadir}/qemu/qemu-kvm.dtb
%{_datadir}/qemu/qemu-tcg.dtb

%changelog
* Thu Jan 18 2024 Eric Curtin <ecurtin@redhat.com>
- Switch from .dtb generation to compiling .dts files. This enables
  accelerated dtbs.

* Wed Sep 13 2023 Eric Curtin <ecurtin@redhat.com>
- Make rpm buildable on Fedora. Fedora doesn't have qemu-kvm on aarch64, but
  has an equivalent

* Mon Mar 13 2023 Eric Curtin <ecurtin@redhat.com>
- Dump only tcg dtb. There's a with_kvm flag you can flip if you want to build.

* Wed Jan 11 2023 Eric Curtin <ecurtin@redhat.com>
- Install dtbs to /usr/share/qemu/ to be ostree compatible
- Dump both tcg and kvm dtbs

* Wed Aug 17 2022 Alexander Larsson <alexl@redhat.com>
- Initial version
