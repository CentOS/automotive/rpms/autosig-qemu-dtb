# autosig-qemu-dtb

This crates a device-tree file (dtb) for use with qemu in combination
with the firmware from `autosig-u-boot`.

Currently aarch64 specific, because of limitations on x86 and devicetree.
